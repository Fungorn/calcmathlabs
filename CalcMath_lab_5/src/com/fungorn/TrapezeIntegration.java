package com.fungorn;


public class TrapezeIntegration implements Integration {
    private int intervals;

    private double integrate(Function f, int intervals, double a, double b) {

        double step = (b - a) / intervals;
        double sum = 0;
        sum += (step / 2) * (f.value(a) + f.value(b));

        for (int i = 1; a + i * step < b; i++) {
            sum += (step * f.value(a + i * step));
        }

        return sum;
    }

    @Override
    public double integrate(Function f, double eps, double a, double b) {
        int power = 1;

        double result;
        do {
            result = integrate(f, (int) Math.pow(2, ++power), a, b);
        } while (Math.abs(integrate(f, (int) Math.pow(2, power - 1), a, b) - result) > eps);
        return result;
    }
}
