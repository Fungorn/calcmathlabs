package com.fungorn;


public class NewtonCotesIntegration implements Integration {
    private double[] weights;
    private double c_0;

    public double integrate(Function f, int k, int m, double a, double b) {
        int n = (int) Math.pow(k, m);
        double h = (b - a) / n;

        double sum = 0;
        switch (k) {
            case 1:
                c_0 = .5;
                weights = new double[]{1, 1};
                for (int i = 0; a + (i + 1) * h <= b; i++) {
                    sum += (c_0 * h * (
                            weights[0] * f.value(a + i * h) +
                            weights[1] * f.value(a + (i + 1) * h)
                    ));
                }
                break;
            case 2:
                c_0 = (1d / 3d);
                weights = new double[]{1, 4, 1};
                for (int i = 0; a + (i + 2) * h <= b; i += 2) {
                    sum += (c_0 * h * (
                            weights[0] * f.value(a + i * h) +
                            weights[1] * f.value(a + (i + 1) * h) +
                            weights[2] * f.value(a + (i + 2) * h)
                    ));
                }
                break;
            case 3:
                c_0 = (3d / 8d);
                weights = new double[]{1, 3, 3, 1};
                for (int i = 0; a + (i + 3) * h <= b; i += 3) {
                    sum += (c_0 * h * (
                            weights[0] * f.value(a + i * h) +
                            weights[1] * f.value(a + (i + 1) * h) +
                            weights[2] * f.value(a + (i + 2) * h) +
                            weights[3] * f.value(a + (i + 3) * h)
                    ));
                }
                break;
            case 4:
                c_0 = (2d / 45d);
                weights = new double[]{7, 32, 12, 32, 7};
                for (int i = 0; a + (i + 4) * h <= b; i += 4) {
                    sum += (c_0 * h * (
                            weights[0] * f.value(a + i * h) +
                            weights[1] * f.value(a + (i + 1) * h) +
                            weights[2] * f.value(a + (i + 2) * h) +
                            weights[3] * f.value(a + (i + 3) * h) +
                            weights[4] * f.value(a + (i + 4) * h)
                    ));
                }
                break;
            case 5:
                c_0 = (5d / 288d);
                weights = new double[]{19, 75, 50, 50, 75, 19};
                for (int i = 0; a + (i + 5) * h <= b; i += 5) {
                    sum += (c_0 * h * (
                            weights[0] * f.value(a + i * h) +
                            weights[1] * f.value(a + (i + 1) * h) +
                            weights[2] * f.value(a + (i + 2) * h) +
                            weights[3] * f.value(a + (i + 3) * h) +
                            weights[4] * f.value(a + (i + 4) * h) +
                            weights[5] * f.value(a + (i + 5) * h)
                    ));
                }
                break;
            default:
                throw new IllegalArgumentException();
        }
        return sum;
    }

    @Override
    public double integrate(Function f, double eps, double a, double b) {
        int k = 3;
        int m = 1;

        double result;
        do {
            result = integrate(f, k, ++m, a, b);
        } while (Math.abs(integrate(f, k, (m - 1), a, b) - result) > eps);
        return result;
    }
}
