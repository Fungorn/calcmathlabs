package com.fungorn;

public class Main {

    public static void main(String[] args) {
        Function f = x -> ( (Math.pow(1 + x, 2)) / (Math.pow(x, 3) * Math.sqrt(2 + x)) );
        double a = 1.4;
        double b = 5.0;

        Integration integral;
        //integral = IntegrationFactory.createMediumRectangleIntegration();
        //integral = IntegrationFactory.createTrapezeIntegration();
        integral = IntegrationFactory.createNewtonCotesIntegration();
        System.out.println(integral.integrate(f, 1e-4, a, b));
    }
}
