package com.fungorn;

public interface Function {
    double value(double x);
}
