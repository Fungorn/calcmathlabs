package com.fungorn;

public interface Integration {
    double integrate(Function f, double eps, double a, double b);
}
