package com.fungorn;

public class IntegrationFactory {
    public static Integration createMediumRectangleIntegration() {
        return new MediumRectangleIntegration();
    }

    public static Integration createTrapezeIntegration() {
        return new TrapezeIntegration();
    }

    public static Integration createNewtonCotesIntegration() {
        return new NewtonCotesIntegration();
    }
}
