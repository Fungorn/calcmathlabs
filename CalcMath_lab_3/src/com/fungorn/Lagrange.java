package com.fungorn;

import cern.colt.matrix.DoubleMatrix1D;

public class Lagrange implements PolynomCalculator {

    public double calculate(double x1, DoubleMatrix1D x, DoubleMatrix1D f) {
        double lagrangePolynom = 0d;

        for (int i = 0; i < x.size(); i++) {
            double b = 1d;
            for (int j = 0; j < x.size(); j++) {
                if (i != j)
                    b *= (x1 - x.getQuick(j)) / (x.getQuick(i) - x.getQuick(j));
            }
            lagrangePolynom += b * f.getQuick(i);
        }
        return lagrangePolynom;
    }
}
