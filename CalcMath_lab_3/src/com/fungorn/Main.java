package com.fungorn;

import cern.colt.matrix.DoubleFactory1D;
import cern.colt.matrix.DoubleMatrix1D;

public class Main {

    public static void main(String[] args) {
        DoubleMatrix1D x = DoubleFactory1D.dense.make(5);
        x.set(0, 1);
        x.set(1, 2);
        x.set(2, 3);
        x.set(3, 4);
        x.set(4, 5);

        DoubleMatrix1D f = DoubleFactory1D.dense.make(5);
        f.set(0, 4);
        f.set(1, 2);
        f.set(2, 8);
        f.set(3, 1);
        f.set(4, -1);

        PolynomCalculatorFactory factory = new PolynomCalculatorFactory();

        double x1 = 3d;

        PolynomCalculator polynomial = factory.createPolynomialCalculator();
        System.out.printf("%.1f\n", polynomial.calculate(x1, x, f));

        PolynomCalculator newton = factory.createNewtonCalculator();
        System.out.println(newton.calculate(x1, x, f));

        PolynomCalculator lagrange = factory.createLagrangeCalculator();
        System.out.println(lagrange.calculate(x1, x, f));
    }
}
