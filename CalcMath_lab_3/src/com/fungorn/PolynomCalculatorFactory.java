package com.fungorn;

public class PolynomCalculatorFactory {
    public PolynomCalculator createPolynomialCalculator() {
        return new Polynomial();
    }

    public PolynomCalculator createNewtonCalculator() {
        return new Newton();
    }

    public PolynomCalculator createLagrangeCalculator() {
        return new Lagrange();
    }
}
