package com.fungorn;

import cern.colt.matrix.DoubleMatrix1D;

public class Newton implements PolynomCalculator {
    private double dividedDiffers(DoubleMatrix1D x, DoubleMatrix1D f, int i, int n) {
        if (n - i == 0) {
            return f.getQuick(i);
        }

        if (n - i == 1) {
            return (f.getQuick(i) - f.getQuick(n))/(x.getQuick(i) - x.getQuick(n));
        }

        if (n - i > 1) {
            return (dividedDiffers(x, f, i, n - 1) - dividedDiffers(x, f, i + 1, n)) / (x.getQuick(i) - x.getQuick(n));
        }
        else return 0;
    }

    public double calculate(double x1, DoubleMatrix1D x, DoubleMatrix1D f) {
        double result = 0d;

        for (int i = 0; i < 5; i++) {
            double c = 1d;
            for (int j = 0; j < i; j++) {
                c *= x1 - x.getQuick(j);
            }
            result += c * dividedDiffers(x, f, 0, i);
        }

        return result;
    }
}
