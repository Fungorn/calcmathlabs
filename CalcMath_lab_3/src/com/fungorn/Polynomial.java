package com.fungorn;

import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;

public class Polynomial implements PolynomCalculator {
    private Algebra algebra = new Algebra();
    private DoubleMatrix2D A;
    private DoubleMatrix2D B;

    private int power = 4;

    private DoubleMatrix2D approximate(DoubleMatrix1D x, DoubleMatrix1D f) {

        A = DoubleFactory2D.dense.make(power + 1, power + 1);
        B = DoubleFactory2D.dense.make(power + 1, 1);

        int n = x.size();
        double asum;
        double bsum;
        for (int i = 0; i < A.rows(); i++) {
            for (int j = 0; j < A.columns(); j++) {
                asum = 0;
                bsum = 0;
                for (int k = 0; k < n; k++) {
                    asum += Math.pow(x.get(k), i + j);
                    double fval = f.get(k);
                    double xval = Math.pow(x.get(k), i);
                    bsum += (fval * xval);
                }
                A.setQuick(i, j, asum);
                B.setQuick(i, 0, bsum);
            }
        }
        return algebra.transpose(algebra.solve(A, B));
    }

    public double calculate(double x1, DoubleMatrix1D x, DoubleMatrix1D f) {
        double[] values = approximate(x, f).toArray()[0];
        double res = 0;

        for (int i = 0; i < values.length; i++) {
            res += values[i] * Math.pow(x1, i);
        }
        return res;
    }
}