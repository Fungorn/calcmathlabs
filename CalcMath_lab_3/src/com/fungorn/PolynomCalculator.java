package com.fungorn;

import cern.colt.matrix.DoubleMatrix1D;

public interface PolynomCalculator {
    double calculate(double x1, DoubleMatrix1D x, DoubleMatrix1D f);
}
