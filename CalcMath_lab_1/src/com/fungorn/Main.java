package com.fungorn;

public class Main {

    interface Function {
        double value(double x);
    }

    public static void main(String[] args) {
        Function function = (x) -> (x * x - 20 * Math.sin(x) - 5);

        SolverFactory factory = new HalfSolverFactory(-6, -4, 1e-6, function, null, null, null);
        Solver solver = factory.createSolver();
        solver.solve(function);
        solver.print();

        /*
        Half half = new Half(-6, -4, 1e-6);
        half.solve(function);
        half.print();
        */
        /*
        Function phi = (x) -> (Math.asin((x * x - 5) / 20));
        Iteration iter = new Iteration(2, 4, 1e-6, phi);
        iter.solve(function);
        iter.print();
        */
        /*
        Newton newton = new Newton(-6, -4, 1e-6, (x) -> (2 * x - 20 * Math.cos(x)), (x) -> (2 + 20 * Math.sin(x)));
        newton.solve(function);
        newton.print();
        */
    }
}
