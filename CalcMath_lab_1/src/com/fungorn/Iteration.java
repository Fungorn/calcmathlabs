package com.fungorn;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Iteration implements Solver {
    private Random random = new Random();
    private List<Double> xs = new ArrayList<>();
    private double xmin, xmax;
    private double eps;
    private Main.Function phi;

    public Iteration(double xmin, double xmax, double eps, Main.Function phi) {
        this.xmin = xmin;
        this.xmax = xmax;
        this.eps = eps;
        this.phi = phi;
    }

    @Override
    public void solve(Main.Function f) {
        solve(f, phi, xmin, xmax, eps);
    }

    private void solve(Main.Function f, Main.Function phi, double xmin, double xmax, double eps) {
        double x_0 = xmin + (xmax - xmin) * random.nextDouble();
        xs.add(x_0);
        int n = 0;

        do {
            double x_n = phi.value(xs.get(n));
            n++;
            xs.add(x_n);
        } while (Math.abs(xs.get(n) - xs.get(n - 1)) > eps);
    }

    @Override
    public void print() {
        System.out.println(xs.get(xs.size() - 1));
    }
}
