package com.fungorn;

public interface Solver {
    void solve(Main.Function function);
    void print();
}
