package com.fungorn;

public interface SolverFactory {
    Solver createSolver();
}
