package com.fungorn;

import java.util.Optional;

public class HalfSolverFactory implements SolverFactory {
    private double xmin, xmax;
    private double eps;
    private Main.Function function;

    public HalfSolverFactory(double xmin, double xmax, double eps, Main.Function function, Optional<Main.Function> firstDerivate, Optional<Main.Function> secondDerivate, Optional<Main.Function> phi) {
        this.xmin = xmin;
        this.xmax = xmax;
        this.eps = eps;
        this.function = function;
    }

    @Override
    public Solver createSolver() {
        return new Half(xmin, xmax, eps);
    }
}
