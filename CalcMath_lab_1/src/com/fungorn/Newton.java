package com.fungorn;

import java.util.ArrayList;
import java.util.List;

public class Newton implements Solver {
    private List<Double> roots = new ArrayList<>();
    private Main.Function firstDerivate;
    private Main.Function secondDerivate;
    private double xmin, xmax;
    private double eps;

    public Newton(double xmin, double xmax, double eps, Main.Function firstDerivate, Main.Function secondDerivate) {
        this.firstDerivate = firstDerivate;
        this.secondDerivate = secondDerivate;
        this.xmin = xmin;
        this.xmax = xmax;
        this.eps = eps;
    }

    @Override
    public void solve(Main.Function function) {
        solve(xmin, xmax, eps, function, firstDerivate, secondDerivate);
    }

    private void solve(double xmin, double xmax, double eps, Main.Function function, Main.Function firstDerivate, Main.Function secondDerivate) {
        double x = 0;
        if (function.value(xmin) * secondDerivate.value(xmin) > 0)
            x = xmin;
        else if (function.value(xmax) * secondDerivate.value(xmax) > 0)
            x = xmax;
        else
            throw new IllegalArgumentException("Wrong interval.");

        double x_1 = x;
        do {
            x = x_1;
            x_1 = interpolateTo(function, firstDerivate, x);
            roots.add(x_1);
        } while (Math.abs(x_1 - x) > eps);
    }

    private double interpolateTo(Main.Function function, Main.Function firstDerivate, double x) {
        return x - function.value(x) / firstDerivate.value(x);
    }

    @Override
    public void print() {
        System.out.println(roots.get(roots.size() - 1));
    }
}
