package com.fungorn;

import java.util.ArrayList;
import java.util.List;

public class Half implements Solver {
    private List<Double> roots = new ArrayList<>();
    private List<Double> xmins = new ArrayList<>();
    private List<Double> xmaxs = new ArrayList<>();
    private double eps;
    private double xmin;
    private double xmax;

    public Half(double xmin, double xmax, double eps) {
        this.eps = eps;
        this.xmin = xmin;
        this.xmax = xmax;
    }

    @Override
    public void solve(Main.Function function) {
        solve(function, xmin, xmax, eps);
    }

    private void solve(Main.Function function, double xmin, double xmax, double eps) {
        double x_0 = (xmin + xmax) / 2;
        double curEps = (xmax - xmin) / 2;

        if (curEps < eps)
            return;
        System.out.println("[" + xmin + ", " + xmax + "]");

        double fmin = function.value(xmin);
        double fcur = function.value(x_0);
        double fmax = function.value(xmax);

        if (Math.abs(function.value(x_0)) < 1e-3) {
            roots.add(x_0);
            xmins.add(xmin);
            xmaxs.add(xmax);
        } else {
            if (fmin * fcur < 0) {
                solve(function, xmin, x_0, eps);
            }
            if (fcur * fmax < 0) {
                solve(function, x_0, xmax, eps);
            }
        }
    }

    @Override
    public void print() {
        for (int i = 0; i < roots.size(); i++) {
            System.out.println("There is root in [" + xmins.get(i) + ", " + xmaxs.get(i) + "]" + " which is " + roots.get(i));
        }
    }
}
