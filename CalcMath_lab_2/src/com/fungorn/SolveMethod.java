package com.fungorn;

import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;

public interface SolveMethod {
    DoubleMatrix1D solve(DoubleMatrix2D A, DoubleMatrix1D B);
}
