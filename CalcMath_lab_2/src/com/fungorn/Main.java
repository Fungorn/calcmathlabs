package com.fungorn;

import cern.colt.matrix.DoubleFactory1D;
import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;

public class Main {

    public static void main(String[] args) {
        DoubleMatrix2D A = DoubleFactory2D.dense.make(3, 3);
        DoubleMatrix1D X;
        DoubleMatrix1D B = DoubleFactory1D.dense.make(3);

        A.set(0, 0, 15);
        A.set(0, 2, 1);
        A.set(1, 0, 4);
        A.set(1, 1, 15);
        A.set(1, 2, 1);
        A.set(2, 0, 1);
        A.set(2, 1, 1);
        A.set(2, 2, 15);

        System.out.println(A);

        B.set(0, 17);
        B.set(1, 16);
        B.set(2, 20);
        SolveMethodFactory solverFactory = new GaussMethodFactory();
        SolveMethod solver = solverFactory.createSolveMethod();
        X = solver.solve(A, B);
        System.out.println("X is: " + X);

        solverFactory = new JacobiMethodFactory();
        solver = solverFactory.createSolveMethod();
        X = solver.solve(A, B);
        System.out.println("X is: " + X);

        // https://dst.lbl.gov/ACSSoftware/colt/api/cern/colt/matrix/linalg/package-summary.html
    }
}
