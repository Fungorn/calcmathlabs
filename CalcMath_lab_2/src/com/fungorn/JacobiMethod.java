package com.fungorn;

import cern.colt.matrix.DoubleFactory1D;
import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;
import cern.colt.matrix.linalg.Algebra;

import java.util.ArrayList;
import java.util.List;

public class JacobiMethod implements SolveMethod {
    private List<DoubleMatrix1D> X = new ArrayList<>();
    private Algebra algebra = new Algebra();

    @Override
    public DoubleMatrix1D solve(DoubleMatrix2D A, DoubleMatrix1D B) {
        DoubleMatrix2D alpha = DoubleFactory2D.dense.make(A.rows(), A.columns());
        DoubleMatrix1D beta = DoubleFactory1D.dense.make(B.size());

        DoubleMatrix1D x;

        for (int i = 0; i < A.rows(); i++) {
            beta.set(i, B.get(i) / A.get(i, i));
            for (int j = 0; j < A.columns(); j++) {
                if (i != j)
                    alpha.set(i, j, - A.getQuick(i, j) / A.getQuick(i, i));
                else
                    alpha.setQuick(i, j, 0);
            }
        }
        x = DoubleFactory1D.dense.make(B.size());
        x.assign(beta);
        X.add(x);

        int k = 0;
        while (true) {
            k++;
            for (int i = 0; i < A.rows(); i++) {
                for (int j = 0; j < A.columns(); j++) {
                    x.set(i, alpha.getQuick(i, j) * x.getQuick(i) + beta.getQuick(i));
                    DoubleMatrix1D x_k = DoubleFactory1D.dense.make(x.size());
                    x_k.assign(x);
                    X.add(x_k);
                }
            }
            DoubleMatrix1D dx = DoubleFactory1D.dense.make(x.size());
            for (int i = 0; i < x.size(); i++) {
                dx.setQuick(i, X.get(k).get(i) - X.get(k - 1).get(i));
            }
            if (Math.sqrt(algebra.norm2(dx)) > (1e-6) * (1 - algebra.norm2(alpha))/algebra.norm2(alpha))
                break;
        }
        return X.get(X.size() - 1);
    }
}
