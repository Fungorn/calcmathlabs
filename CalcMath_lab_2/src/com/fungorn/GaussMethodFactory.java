package com.fungorn;

public class GaussMethodFactory implements SolveMethodFactory {
    @Override
    public SolveMethod createSolveMethod() {
        return new GaussMethod();
    }
}
