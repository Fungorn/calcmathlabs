package com.fungorn;

import cern.colt.matrix.DoubleFactory1D;
import cern.colt.matrix.DoubleFactory2D;
import cern.colt.matrix.DoubleMatrix1D;
import cern.colt.matrix.DoubleMatrix2D;

public class GaussMethod implements SolveMethod {
    private DoubleMatrix2D augA;

    private void forwardGaussMethod() {
        for (int i = 0; i < augA.rows(); i++) {
            double aii = augA.viewRow(i).get(i);
            for (int j = 0; j < augA.columns(); j++) {
                if (Math.abs(aii) < 1e-13) {
                    for (int k = 0; k < augA.rows(); k++) {
                        if (!(Math.abs(augA.get(k, k)) < 1e-13)) {
                            swapRows(i, k);
                        }
                    }
                }
                double aij = augA.viewRow(i).get(j);
                augA.set(i, j, aij / aii);
            }
            for (int j = i + 1; j < augA.rows(); j++) {
                calculateRow(i, j);
            }
        }
    }

    private void backwardGaussMethod() {
        for (int i = augA.rows() - 1; i > 0; i--) {
            for (int j = i - 1; j >= 0 ; j--) {
                calculateRow(i, j);
            }
        }
    }

    private void calculateRow(int i, int j) {
        double m = augA.getQuick(j, i) / augA.getQuick(i, i);
        for (int k = 0; k < augA.columns(); k++) {
            augA.set(j, k, augA.getQuick(j ,k) - m * augA.getQuick(i, k));
        }
    }

    @Override
    public DoubleMatrix1D solve(DoubleMatrix2D A, DoubleMatrix1D B) {

        augA = DoubleFactory2D.dense.make(A.rows(), A.columns() + 1);
        augA.viewPart(0, 0, A.rows(), A.columns()).assign(A);
        augA.viewColumn(A.columns()).assign(B);

        forwardGaussMethod();
        backwardGaussMethod();

        return augA.viewColumn(augA.columns() - 1);
    }

    private void swapRows(int i, int i1) {
        DoubleMatrix1D temp = DoubleFactory1D.dense.make(augA.columns());
        temp.assign(augA.viewRow(i));
        augA.viewRow(i).assign(augA.viewRow(i1));
        augA.viewRow(i1).assign(temp);
    }
}
