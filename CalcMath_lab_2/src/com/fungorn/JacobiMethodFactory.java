package com.fungorn;

public class JacobiMethodFactory implements SolveMethodFactory {
    @Override
    public SolveMethod createSolveMethod() {
        return new JacobiMethod();
    }
}
