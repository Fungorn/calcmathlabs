package com.fungorn;

public interface SolveMethodFactory {
    SolveMethod createSolveMethod();
}
