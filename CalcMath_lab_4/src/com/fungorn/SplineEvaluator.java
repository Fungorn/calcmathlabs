package com.fungorn;

import cern.colt.matrix.DoubleMatrix1D;

import java.util.ArrayList;
import java.util.List;

public class SplineEvaluator {
    private DoubleMatrix1D x;
    private DoubleMatrix1D y;

    private double h;

    private List<Spline> splines = new ArrayList<>();

    public SplineEvaluator(DoubleMatrix1D x, DoubleMatrix1D y, double a, double b, int n) {
        this.x = x;
        this.y = y;

        this.h = (b - a) / n;

        Spline s0 = new Spline(0, h);
        s0.a = 0;
        s0.c = 0;
        Spline sn = new Spline(b - h, b);

        splines.add(s0);
        splines.add(sn);
    }
}
